<?php

	//運行指定文件
	include("database.php");
	//啟用Session
	session_start();

	//帳號
	$account = trim($_POST["account"]);
	//密碼
	$password = md5(trim($_POST["password"]));
	//權限
	$level = "1";
	//若輸入空值可以先在此阻擋
	if($account == ""){
		echo "請輸入帳號,若無帳號請先註冊";
		echo '<meta http-equiv="refresh" content="2;url=http://localhost/shopping/Register.html">';
		exit;
	}

	try{
		//建立連線pdo
		$conn = new PDO($dsn,$dbuser,$dbpasswd);
		$conn->exec("SET CHARACTER SET utf8");
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		//查詢
		$stmt = $conn -> prepare("select id,level,account,password from signin WHERE (account=:account and password=:password)");
		$stmt->bindParam(":account", $account,PDO::PARAM_STR) ;
		$stmt->bindParam(":password", $password,PDO::PARAM_STR) ;
		$stmt->execute();	
		//$data=$stmt->fetchall(PDO::FETCH_ASSOC);
		$data=$stmt->fetch(PDO::FETCH_ASSOC);

		//print_r($data['level']);

		if(!empty($data)){

			if($data['level'] == 1){
				 //當權限為1就導入index.php=最高權限者
				 $_SESSION['user_id']=$data['id'];
				 //echo $_SESSION['id'];
				 $_SESSION['account']=$data['account'];//使用者帳號
				 //echo $_SESSION['account'];
				 $_SESSION['level']=$data['level'];
				// header("location: index.php");
				header("location: home.php");
			 }
			if($data['level'] == 2){
				 //當權限為2就導入home.php=一般普通會員
				 $_SESSION['user_id']=$data['id'];
				 //echo $_SESSION['id'];
				 $_SESSION['account']=$data['account'];//使用者帳號
				 $_SESSION['level']=$data['level'];
                 header("location: home.php");
			}else{
				//如果沒有顯示錯誤
				echo 'error';
			}

		}else{
			//導頁
			//echo 'error';
			header("location: Error.html");
			
		}
	}catch(PDOException $e){
		echo "Connection failed: ".$e->getMessage();
	} finally{
	    $conn = null;	
	}

	
	
?>