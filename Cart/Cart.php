<?php
        //啟用session
        session_start();

        if($_SESSION['user_id'] == ""){
            echo "你無權進來.給我滾";
            header("location: login.html");
        }

        //運行指定文件
        include("../database.php");

        //使用者
        $userid=$_SESSION['user_id'];

        //進行資料庫PDO連線
        $conn = new PDO($dsn,$dbuser,$dbpasswd);
        $conn->exec("SET CHARACTER SET utf8");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "Connected Successfully";

        //用使用者ID從購物車資料表撈出商品編號
        $stmt = $conn -> prepare("select product_id from `cart` where $userid ");
        $stmt->execute();	
        //$data=$stmt->fetchall(PDO::FETCH_ASSOC);
        $result=$stmt->fetchall(PDO::FETCH_ASSOC);

        //判斷購物車有沒有值
        if(empty($result)){
            //如果沒有：
            // echo '購物車目前沒有商品';
            // echo "<td>"."<input type='button' value='回首頁' onclick=location.href='../home.php'>"."</td>";
        }else{
            //有,執行以下動作,用商品編號從商品資料表撈出商品資料,因為商品編號逗號關係,我用for迴圈count陣列長度
            //在加上用字串相加的方式來組成sql指令,為的就是那個麻煩的逗號,題外話：打註解蠻好玩的XD
            $sqlstr = "select * from `commodity` where `id` in ( ";
            for($i=0;$i<count($result);$i++){
                //下面就是測試看跑出來是什麼@@
                //echo  $result[$i]['product_id'];
                $sqlstr .= $result[$i]['product_id'];
                if($i+1 != count($result)){
                $sqlstr .= ",";
                }
            }
            //php字串相加為(.=)那麼JavaScript呢就是(+)
            $sqlstr .= ")";
            //執行$sqlstr跑結果摟
            $stmt = $conn->prepare($sqlstr);
            $stmt->execute();

            $product=$stmt->fetchall(PDO::FETCH_ASSOC);
            //print_r($product);echo "<br>";

            //下面為商品總計,我假設是0,因為目前未知,設定為空值又怪怪的
            //所以我就設定為0
            $total = 0;

            // //印出table
            // echo"<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>'";
            // echo "<h2 align='center'>我的購物車</h2>";
            //         //form表單方式進行傳遞
            // echo "<form action='Surebuy.php' method='post' onSubmit='return CheckForm()';>";
            // echo "<table align='center' style='border: solid 1px black;text-align:center; border:3px #cccccc solid;'>";
            // echo
            // "<tr>
            //     <th>商品編號</th>
            //     <th>商品名稱</th>
            //     <th>商品圖片</th>
            //     <th>商品價格</th>
            //     <th>商品數量</th>
            //     <th>商品小計</th>
            //     <th>操作功能</th>
            // </tr>";
            // foreach ($product as $value){ 
            //     echo "<tr>"; 
            //     echo    "<td>";//下面為商品編號,隱藏商品編號欄位,我以form表單方式傳遞值.
            //     echo        '<input type="hidden" name="id[]" value="'.$value['id'].'" />';
            //     echo        $value['id'];   //上面的命名代表它唯一陣列：XXX[].
            //     echo    "</td>"; 
            //     echo    "<td>";//下面為商品名稱,隱藏商品名稱欄位,我以form表單方式傳遞值.
            //     echo        '<input type="hidden" name="title[]" value="'.$value['title'].'" />';
            //     echo        $value['title'];
            //     echo    "</td>";
            //     echo    "<td>";//下面為商品圖片,隱藏商品名稱欄位,我以form表單方式傳遞值.
            //     echo        '<input type="hidden" name="image[]" value="'.$value['image'].'" />';
            //     echo        "<img src='../Image/".$value['image']."'width='200' height='100'>";
            //     echo    "</td>";
            //     echo    "<td>";//下面為商品價格,隱藏商品價格欄位,我以form表單方式傳遞值.
            //     echo        '<input type="hidden" name="jiage[]" value="'.$value['price'].'" />';
            //     echo        "<div id='jiage".$value['id']."'>".$value['price']."</div>";

            //     echo    "</td>";
            //                    //下面為商品數量,以下拉式的方式來做,我以form表單方式傳遞值.
            //     echo    "<td><select id=num".$value['id']." onchange='print_value(".$value['id'].");' name='enough[]'>";
            //                    //如果庫存量大於0,我就跑for迴圈,直到跟庫存相同
            //                 if($value['enough'] > 0){
            //                     for($i=1;$i<=($value['enough']);$i++){
            //                         echo  "<option value =".$i." >".$i."</option>";
            //                     }
            //                     echo "</select>";
            //                 }else{
            //                    //如果沒有,我就顯示無庫存
            //                     echo "<span>無庫存</span>";
            //                 }   

            //     echo    "</td>";

            //     $total =  $total + $value['price'];

            //     echo    "<td>";//下面為商品小計.
            //     echo           "<div id='price".$value['id']."'>".$value['price']."</div>";
            //     echo    "</td>";
            //                    //以下為刪除,觸發javascript導入事件,就是現在文字底下@.@
            //     echo    "<td>".'<input type="button" onclick="ConfirmDelete('.$value['id'].')" value="刪除">'."</td>";
            //     echo "</tr>"; 
            // }
            //     echo "<tr>"; 
            //     echo    "<td>"."<input type='button' value='繼續選購商品' onclick=location.href='../Commodity/SeeList.php'>"."</td>";
            //     echo    "<td>"."<input type='button' value='回首頁' onclick=location.href='../home.php'>"."</td>";
            //     echo    "<td></td>";
            //     echo    "<td></td>";
            //     echo    "<td><span>總金額</span></td>";
            //     echo    "<td>";
            //                 //下面為總金額,設定為無法更改(readonly='readonly'),我用text的方式來顯示,比較方便QQ
            //     echo    "<div><input type='text' readonly='readonly' id='totals' name='totals' value='".$total."' /></div>";
            //     echo    "</td>";
            //     echo    "<td><input type='submit' value='確定購買'>"."</td>"; 
            //     echo "</tr>"; 


            //     echo "</table>";//table結束
            //     echo "</form>"; //form結束

            }   

?>

            <!-- 下面為點選刪除所觸發的事件 -->
            <script type="text/javascript">
            function ConfirmDelete(cart_id)
            {
                //alert(commodity_id);

                if (confirm("確定要刪除？")){
                    location.href = 'Delete.php?id='+cart_id;
                }
            }
            
                //下面為庫存的事件
           function print_value(id) {
                <!-- 將 select 的值在印出 -->
                var num = "num"+id;
                var enough  = document.getElementById(num).value;
                //console.log(t);
                $('#'+num).attr('name','enough[]=>'+enough+'');


                //下面為價格與小計
                //jiage價格
                var jiage;
                jiage = $('#jiage'+id).text();
                //console.log(jiage);

                var price ;
                price = jiage * enough;
                //console.log(price)

                $('#price'+id).text(price);

                //總價格,直接用下面的觸發
                money_total();

    
            }
                //總價格的Function
            function money_total(){
                var total = Array();
                $("div[id^='price']").each(function() {

                    total.push($(this).text())

                });
                
                var totals = parseInt(0);
                for(i=0;i<total.length;i++){
                    totals += parseInt(total[i]);
                }
                console.log(totals);

                //$('#total').text(totals);
                $('#totals').val(totals);

            }

                function CheckForm()

                    {

                    if(confirm("確認商品是否正確,若無疑問,請點選確認")==true)   

                        return true;

                    else  

                        return false;

                    }   

            </script>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Hong-Kai購物網</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="http://localhost/shopping/home.php">首頁</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"></li>
        <li><a href="http://localhost/shopping/Cart/Cart.php">購物車</a></li>
        <li><a href="http://localhost/shopping/News/SeeList.php">消息</a></li>
        <li><a href="http://localhost/shopping/Member/SeeList.php">會員</a></li>
        <li><a href="http://localhost/shopping/Commodity/SeeList.php">商品</a></li>
        <li><a href="http://localhost/shopping/Cart/MyOrder.php">訂單</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <?php
          if($_SESSION['level'] == 1){
            echo ' <li><a href="http://localhost/shopping/index.php"><span class="glyphicon glyphicon-log-in"></span> 管理</a></li>';
          }  
        ?>
        <li><a href="http://localhost/shopping/Signout.php"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container text-center">    
  <div class="row content">
    <div class="col-sm-12 text-left"> 
        <div class="table-responsive">
          <form action='Surebuy.php' method='post' onSubmit='return CheckForm()';>           
            <table class="table">
            <h1>我的購物車清單<span class="badge badge-secondary">CartProducts</span></h1>   
              <thead>
                <tr>
                <th class="text-center">商品編號</th>
                <th class="text-center">商品名稱</th>
                <th class="text-center">商品圖片</th>
                <th class="text-center">商品價格</th>
                <th class="text-center">商品數量</th>
                <th class="text-center">商品小計</th>
                <th class="text-center">操作功能</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($product as $value){ 
                    echo "<tr>"; 
                    echo    "<td class='text-center'>";//下面為商品編號,隱藏商品編號欄位,我以form表單方式傳遞值.
                    echo        '<input type="hidden" name="id[]" value="'.$value['id'].'" />';
                    echo        $value['id'];   //上面的命名代表它唯一陣列：XXX[].
                    echo    "</td>"; 
                    echo    "<td class='text-center'>";//下面為商品名稱,隱藏商品名稱欄位,我以form表單方式傳遞值.
                    echo        '<input type="hidden" name="title[]" value="'.$value['title'].'" />';
                    echo        $value['title'];
                    echo    "</td>";
                    echo    "<td class='text-center'>";//下面為商品圖片,隱藏商品名稱欄位,我以form表單方式傳遞值.
                    echo        '<input type="hidden" name="image[]" value="'.$value['image'].'" />';
                    echo        "<img src='../Image/".$value['image']."'width='200' height='100'>";
                    echo    "</td>";
                    echo    "<td class='text-center'>";//下面為商品價格,隱藏商品價格欄位,我以form表單方式傳遞值.
                    echo        '<input type="hidden" name="jiage[]" value="'.$value['price'].'" />';
                    echo        "<div id='jiage".$value['id']."'>".$value['price']."</div>";
    
                    echo    "</td>";
                                   //下面為商品數量,以下拉式的方式來做,我以form表單方式傳遞值.
                    echo    "<td class='text-center'><select id=num".$value['id']." onchange='print_value(".$value['id'].");' name='enough[]'>";
                                   //如果庫存量大於0,我就跑for迴圈,直到跟庫存相同
                                if($value['enough'] > 0){
                                    for($i=1;$i<=($value['enough']);$i++){
                                        echo  "<option value =".$i." >".$i."</option>";
                                    }
                                    echo "</select>";
                                }else{
                                   //如果沒有,我就顯示無庫存
                                    echo "<span>無庫存</span>";
                                }   
    
                    echo    "</td>";
    
                    $total =  $total + $value['price'];
    
                    echo    "<td class='text-center'>";//下面為商品小計.
                    echo           "<div id='price".$value['id']."'>".$value['price']."</div>";
                    echo    "</td>";
                                   //以下為刪除,觸發javascript導入事件,就是現在文字底下@.@
                    echo    "<td class='text-center'>".'<input type="button" class="btn btn-primary" onclick="ConfirmDelete('.$value['id'].')" value="刪除">'."</td>";
                    echo "</tr>"; 
                    }
                    echo "<tr>"; 
                    echo    "<td class='text-center'>"."<input type='button' class='btn btn-primary' value='繼續選購商品' onclick=location.href='../Commodity/SeeList.php'>"."</td>";
                    echo    "<td class='text-center'>"."<input type='button' class='btn btn-primary' value='回首頁' onclick=location.href='../home.php'>"."</td>";
                    echo    "<td></td>";
                    echo    "<td></td>";
                    echo    "<td class='text-center'><span>總金額</span></td>";
                    echo    "<td>";
                                //下面為總金額,設定為無法更改(readonly='readonly'),我用text的方式來顯示,比較方便QQ
                    echo    "<div><input type='text' class='text-center' readonly='readonly' id='totals' name='totals' value='".$total."' /></div>";
                    echo    "</td>";
                    echo    "<td class='text-center'><input type='submit' class='btn btn-primary' value='確定購買'>"."</td>"; 
                    echo "</tr>"; 


                ?>
                
              </tbody>
            </table>
           </form> 
          </div> 
    </div>
  </div>
</div>


<footer class="container-fluid text-center">
  <p>如有其他需求請寄信至：hongkai@program.com.tw</p>
</footer>

</body>
</html>

