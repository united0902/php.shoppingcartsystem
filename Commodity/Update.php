<?php


    //運行指定文件
    include("../database.php");

    $id = $_GET["id"];
    //echo $id;

try
{
    //進行資料庫PDO連線
    $conn = new PDO($dsn,$dbuser,$dbpasswd);
    $conn->exec("SET CHARACTER SET utf8");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected Successfully";

    //查詢
    $stmt = $conn->prepare("SELECT * FROM commodity where (id=:id)");
    $stmt->bindParam(":id", $id,PDO::PARAM_STR) ;
    $stmt->execute();
    $result=$stmt->fetchall(PDO::FETCH_ASSOC);
    $value  = $result[0];
    
    }catch(PDOException $e){
        echo "Connection failed: ".$e->getMessage();
    }finally{
     $conn = NULL;
     echo "</table>";
}

?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">

		<!-- Website CSS style -->
		<link rel="stylesheet" type="text/css" href="assets/css/main.css">

		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

		<title>商品修改</title>
	</head>
	<body>
		<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">購物網商品修改</h1>
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
					<form class="form-horizontal" method="post" action="Set.php">
						
						<div class="form-group">
							<label for="commodity_id" class="cols-sm-2 control-label">商品編號(此項目無法更改)</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="hidden" name="commodity_id" id="commodity_id" placeholder="請輸入商品編號" value = <?php echo "$value[id]";?>>
                                    <input type="text" class="form-control" disabled placeholder="請輸入商品編號" value = <?php echo "$value[id]";?>>
								</div>
							</div>
                        </div>
                        
                        <div class="form-group">
							<label for="title" class="cols-sm-2 control-label">商品名稱</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="title" id="title" placeholder="請輸入商品名稱" value=<?php echo "$value[title]";?>>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="kind" class="cols-sm-2 control-label">商品類型</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <select class="form-control" name="kind" id="kind"  placeholder="請選擇商品種類">
                                    <option selected><?php echo "$value[kind]";?>
                                    <option>服飾類
                                    <option>鞋類
                                    <option>3c類
                                    <option>日常用品類
                                    </select>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label for="price" class="cols-sm-2 control-label">商品價格</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="price" id="price"  placeholder="請輸入商品金額" value=<?php echo "$value[price]";?>>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="enough" class="cols-sm-2 control-label">商品庫存</label>
							<div class="cols-sm-10">
								<div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="enough" id="enough"  placeholder="請輸入商品庫存" value=<?php echo "$value[enough]";?>>
								</div>
							</div>
						</div

						<div class="form-group">
							<label for="image" class="cols-sm-2 control-label">商品圖片</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="file" class="form-control" name="image" id="image"  placeholder="請輸入上傳圖片" value=<?php echo "$value[image]";?>>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label for="content" class="cols-sm-2 control-label">商品描述</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<textarea rows='4' cols='50' class="form-control" name="content" id="content"  placeholder="請輸入商品描述"><?php echo "$value[content]";?></textarea>
								</div>
							</div>
						</div>

						<div class="form-group ">
							<button type="submit" class="btn btn-primary btn-lg btn-block login-button">確定修改</button>
							<button type="reset" class="btn btn-primary btn-lg btn-block login-button">清除重填</button>
						</div>
						<div class="login-register">
				            <a href="http://localhost/shopping/Commodity/index.php">返回商品管理</a>
				         </div>
					</form>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	</body>
</html>