<?php

        //運行指定文件
        include("../database.php");



		//商品類型
		$kind = $_POST["kind"]; //echo $kind;

		if($kind=="請選擇商品類型"){
			echo "請輸入商品類型";
			echo '<meta http-equiv="refresh" content="2;url=http://localhost/shopping/Commodity/Createkind.html">';
			exit;
		}

	try
	{
		//進行PDO連線
		$conn = new PDO($dsn,$dbuser,$dbpasswd);
		$conn->exec("SET CHARACTER SET utf8");
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//echo "Connected Successfully";

		$stmt = $conn -> prepare("select kind from productkind WHERE (kind=:kind)");
		$stmt->bindParam(":kind", $kind,PDO::PARAM_STR) ;
		$stmt->execute();
		$result=$stmt->fetch(PDO::FETCH_ASSOC);;
		if(!empty($result)){
			echo "商品名稱重複,請重新輸入";
			echo '<meta http-equiv="refresh" content="2;url=http://localhost/shopping/Commodity/Createkind.html">';
		}else{
    
        //向資料表新增資料
	 	$stmt = $conn -> prepare("insert into `productkind` (`kind`) values (:kind)");
		
		//防止亂碼
         $stmt->bindParam(':kind', $kind) ;

		
	 	$stmt->execute();	
	 	//echo "新增成功"; 
		 echo "新增成功.....2秒後幫你轉跳頁面";
		//導頁
	 	echo '<meta http-equiv="refresh" content="2;url=http://localhost/shopping/Commodity/ProductKind.php">';
		
		}

	 }catch(PDOException $e){
		//連線失敗直接中斷並告知原因
	 	echo "Connection failed: ".$e->getMessage();
	 }finally{
		$conn = NULL;
	 }
	
?>