<?php


	//運行指定文件
	include("../database.php");

	//創建消息標題
	$newstitle = $_POST["newstitle"];
	//創建消息關鍵字
	$newskeyword = $_POST["newskeyword"];
	//創建消息內容
	$newscontent = $_POST["newscontent"];

	if($newstitle==""){
		echo "請輸入標題名稱";
		echo '<meta http-equiv="refresh" content="2;url=http://localhost/shopping/News/Create.html">';
		exit;
	}
	if($newstitle==""){
		echo "請輸入消息關鍵字";
		echo '<meta http-equiv="refresh" content="2;url=http://localhost/shopping/News/Create.html">';
		exit;
	}
	if($newscontent==""){
		echo "請輸入消息內容";
		echo '<meta http-equiv="refresh" content="2;url=http://localhost/shopping/News/Create.html">';
		exit;
	}

	
	try
	{
		//進行資料庫PDO連線
		$conn = new PDO($dsn,$dbuser,$dbpasswd);
		$conn->exec("SET CHARACTER SET utf8");
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//echo "Connected Successfully";

		$stmt = $conn -> prepare("select newstitle from createnews WHERE (newstitle=:newstitle)");
		$stmt->bindParam(":newstitle", $newstitle,PDO::PARAM_STR) ;
		$stmt->execute();
		$result=$stmt->fetch(PDO::FETCH_ASSOC);;
		if(!empty($result)){
			echo "此標題已重複新增,請重新輸入標題名稱,謝謝";
			echo '<meta http-equiv="refresh" content="2;url=http://localhost/shopping/News/Create.html">';
		}else{
		
		//匯入資料
	 	$stmt = $conn -> prepare("insert into `createnews` (newstitle,newskeyword,newscontent)
							 values(:newstitle,:newskeyword,:newscontent)");
							         					
         $stmt->bindParam(':newstitle', $newstitle) ;
         $stmt->bindParam(':newskeyword', $newskeyword) ;
         $stmt->bindParam(':newscontent', $newscontent) ;
		
         $stmt->execute();
         	
	 	//echo "新增成功"; 
	 	echo "新增成功.....2秒後幫你轉跳頁面";
	 	echo '<meta http-equiv="refresh" content="2;url=http://localhost/shopping/News/index.php">';

		}
	 }catch(PDOException $e){
	 	echo "Connection failed: ".$e->getMessage();
	 }finally{
		$conn = NULL;
	 }
	
?>