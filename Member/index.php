
         <script type="text/javascript">
            function ConfirmDelete(signin_id)
            {
                //alert(commodity_id);

                if (confirm("確定要刪除？")){
                    location.href = 'Delete.php?id='+signin_id;
                }
            }
        </script>

<?php
    session_start();
    if($_SESSION['user_id'] == ""){
      echo "你無權進來.給我滾";
      header("location: login.html");
  }

    //運行指定文件
    include("../database.php");

try
{

    //進行資料庫PDO連線
    $conn = new PDO($dsn,$dbuser,$dbpasswd);
    $conn->exec("SET CHARACTER SET utf8");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected Successfully";

    //從資料表撈資料
    $stmt = $conn->prepare("SELECT * FROM signin");
    $stmt->execute();
    $result=$stmt->fetchall(PDO::FETCH_ASSOC);
        
}catch(PDOException $e){
    echo "Connection failed: ".$e->getMessage();
}finally{
   $conn = NULL;
   echo "</table>";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Hong-Kai購物網</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="http://localhost/shopping/index.php">首頁</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"></li>
        <li><a href="http://localhost/shopping/News/index.php">消息管理</a></li>
        <li><a href="http://localhost/shopping/Member/index.php">會員管理</a></li>
        <li><a href="http://localhost/shopping/Cart/Order.php">訂單管理</a></li>
        <li class="dropdown">
				<a href="http://localhost/shopping/Commodity/index.php" class="dropdown-toggle" data-toggle="dropdown">
					商品管理 
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
          <li><a href="http://localhost/shopping/Commodity/index.php">所有商品</a></li>
          <li><a href="http://localhost/shopping/Commodity/ProductKind.php">商品種類</a></li>
				</ul>
			  </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <?php
          if($_SESSION['level'] == 1){
            echo ' <li><a href="http://localhost/shopping/home.php"><span class="glyphicon glyphicon-log-in"></span> 回客戶畫面</a></li>';
          }  
        ?>
        <li><a href="http://localhost/shopping/Signout.php"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container text-center">    
  <div class="row content">
    <div class="col-sm-12 text-left"> 
        <div class="table-responsive">          
            <table class="table">
            <h1>所有會員<span class="badge badge-secondary">Member</span></h1>   
              <thead>
                <tr>
                <th class="text-center">編號</th>
                <th class="text-center">帳號</th>
                <th class="text-center">密碼</th>
                <th class="text-center">姓名</th>
                <th class="text-center">性別</th>
                <th class="text-center">信箱</th>
                <th class="text-center">地址</th>
                <th class="text-center">電話</th>
                <th class="text-center">操作</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($result as $value){ 
                    echo "<tr>"; 
                    echo "<td class='text-center'>".$value['id']."</td>"; 
                    echo "<td class='text-center'>".$value['account']."</td>"; 
                    echo "<td class='text-center'>".$value['password']."</td>"; 
                    echo "<td class='text-center'>".$value['name']."</td>";
                    echo "<td class='text-center'>".$value['sex']."</td>";
                    echo "<td class='text-center'>".$value['mail']."</td>";
                    echo "<td class='text-center'>".$value['adds']."</td>";
                    echo "<td class='text-center'>".$value['phone']."</td>";
                    echo "<td class='text-center'>"."<input type='button'  class='btn btn-warning' value='修改' onclick=location.href='Update.php?id=".$value['id']."'>"."</td>";   
                    echo "<td class='text-center'>".'<input type="button"  class="btn btn-danger" onclick="ConfirmDelete('.$value['id'].')" value="刪除">'."</td>"; 
                    echo "</tr>"; 
                    }
                    echo "<td class='text-center'>"."<input type='button'  class='btn btn-primary' value='新增會員' onclick=location.href='Create.html'>"."</td>";
                    echo "<td class='text-center'>"."<input type='button'  class='btn btn-primary' value='回管理者首頁' onclick=location.href='../index.php'>"."</td>";
                ?>
                
              </tbody>
            </table>
          </div> 
    </div>
  </div>
</div>


<footer class="container-fluid text-center">
  <p>如有其他需求請寄信至：hongkai@program.com.tw</p>
</footer>

</body>
</html>