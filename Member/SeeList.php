<?php
    session_start();
    if($_SESSION['user_id'] == ""){
      echo "你無權進來.給我滾";
      header("location: login.html");
  }

    //運行指定文件
    include("../database.php");

try
{

    //進行資料庫PDO連線
    $conn = new PDO($dsn,$dbuser,$dbpasswd);
    $conn->exec("SET CHARACTER SET utf8");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected Successfully";

    //去資料表撈資料
    $stmt = $conn->prepare("SELECT * FROM signin");
    $stmt->execute();
    $result=$stmt->fetchall(PDO::FETCH_ASSOC);
        
}catch(PDOException $e){
    echo "Connection failed: ".$e->getMessage();
}finally{
   $conn = NULL;
   echo "</table>";
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Hong-Kai購物網</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="http://localhost/shopping/home.php">首頁</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"></li>
        <li><a href="http://localhost/shopping/Cart/Cart.php">購物車</a></li>
        <li><a href="http://localhost/shopping/News/SeeList.php">消息</a></li>
        <li><a href="http://localhost/shopping/Member/SeeList.php">會員</a></li>
        <li><a href="http://localhost/shopping/Commodity/SeeList.php">商品</a></li>
        <li><a href="http://localhost/shopping/Cart/MyOrder.php">訂單</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <?php
          if($_SESSION['level'] == 1){
            echo ' <li><a href="http://localhost/shopping/index.php"><span class="glyphicon glyphicon-log-in"></span> 管理</a></li>';
          }  
        ?>
        <li><a href="http://localhost/shopping/Signout.php"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container text-center">    
  <div class="row content">
    <div class="col-sm-12 text-left"> 
        <div class="table-responsive">          
            <table class="table">
            <h1>所有會員<span class="badge badge-secondary">Member</span></h1>  
              <thead>
                <tr>
                <th class="text-center">編號</th>
                <th class="text-center">帳號</th>
                <th class="text-center">姓名</th>
                <th class="text-center">性別</th>
                <th class="text-center">信箱</th>
                <th class="text-center">地址</th>
                <th class="text-center">電話</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($result as $value){ 
                    echo "<tr>"; 
                    echo "<td class='text-center'>".$value['id']."</td>"; 
                    echo "<td class='text-center'>".$value['account']."</td>"; 
                    echo "<td class='text-center'>".$value['name']."</td>";
                    echo "<td class='text-center'>".$value['sex']."</td>";
                    echo "<td class='text-center'>".$value['mail']."</td>";
                    echo "<td class='text-center'>".$value['adds']."</td>";
                    echo "<td class='text-center'>".$value['phone']."</td>";
                    echo "</tr>"; 
                    }
                    echo "<td class='text-center'>"."<input type='button'  class='btn btn-primary' value='搜尋會員' onclick=location.href='Select.html'>"."</td>";
                ?>
                
              </tbody>
            </table>
          </div> 
    </div>
  </div>
</div>


<footer class="container-fluid text-center">
  <p>如有其他需求請寄信至：hongkai@program.com.tw</p>
</footer>

</body>
</html>